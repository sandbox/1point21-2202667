The Flowdock API module does not provide any functionality 
on its own instead it is a base module on which other 
modules can be built.  The module is built using 
Flowdock's REST API and performs it communication via 
PHP's CURL library.  If enough demand surfaces in the 
future I may port over the HTTP communication layer to use 
Drupal's HTTP Request object or may it switchable via 
the module settings.

If you would like to extend the base class provided by the 
Flowdock API module you will need to mark the Flowdock API 
module as a dependency in your module's .info file. The Flowdock API 
module at this time provides thea PHP class called FlowdockApiCurl 
which you can extend to automate Drupal's settings for your 
module or may create your objects directly directly inputing the 
settings.

An example of how to extend the Flowdock API module can be found 
in the Flowdock Log module.
 