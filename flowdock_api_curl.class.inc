<?php
/**
 * @file
 * Contains \FlowdockAPI\Curl.
 */

/**
 * The CURL implementation of the Flowdock API class.
 */
class FlowdockApiCurl extends FlowdockApi {

  /**
   * The curl options for curl_setopt.
   *
   * @var array
   */
  protected $httpRequestOptions = array(
    CURLOPT_HEADER => FALSE,
    CURLOPT_TIMEOUT => 10,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HTTPHEADER => array(
      'Accept: application/json'
    )
  );

  /**
   * {@inheritdoc}
   */
  protected function initHttpRequest($path = NULL, $method = NULL, $data = NULL, $auth = TRUE) {
    $url = $this->getHttpUrl();
    if(!empty($path)) {
      $url .= '/'.$path;
    }
    $this->httpRequest = curl_init($url);
    $options = $this->httpRequestOptions;
    //Handle method specific setup for non get requests
    switch($method) {
      case self::HTTP_POST:
        //Handle a HTTP POST request
        $post = http_build_query($data);
        curl_setopt($this->httpRequest, CURLOPT_POST, TRUE);
        curl_setopt($this->httpRequest, CURLOPT_POSTFIELDS, $post);
        $options[] = 'Content-Length: ' . strlen($post);
        break;
      case self::HTTP_PUT:
        //Send a HTTP PUT request to the URL
        $post = http_build_query($data);
        curl_setopt($this->httpRequest, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($this->httpRequest, CURLOPT_POSTFIELDS, $post);
        $options[] = 'Content-Length: ' . strlen($post);
        break;
      case self::HTTP_DELETE:
        //Send a HTTP DELETE request to the URL
        curl_setopt($this->httpRequest, CURLOPT_CUSTOMREQUEST, "DELETE");
        break;
    }
    //Assemble all of the options from above
    curl_setopt_array($this->httpRequest,$options);
    //Setup Basic Auth
    if($auth) {
      curl_setopt($this->httpRequest, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      //Setup username and password
      curl_setopt($this->httpRequest, CURLOPT_USERPWD, $this->userApiToken.":DUMMY");
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function sendHttpRequest($path = NULL, $method = NULL, $data = NULL, $auth = TRUE) {
    $this->initHttpRequest($path,$method,$data);
    $this->httpRequestResponse = curl_exec($this->httpRequest);
    $this->httpRequestError = curl_error($this->httpRequest);
    $code = curl_getinfo($this->httpRequest,CURLINFO_HTTP_CODE);
    $this->closeHttpRequest();
    if(!empty($this->httpRequestError)) {
      throw new FlowdockApiHttpException($this->httpRequestError);
    }
    if($code == 401) {
      throw new FlowdockApiHttpAuthException($this->httpRequestResponse,$code);
    }
    if($code < 200 || $code > 299) {
      throw new FlowdockApiHttpException($this->httpRequestResponse,$code);
    }
    if(!empty($this->httpRequestResponse)) {
      return json_decode($this->httpRequestResponse);
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function closeHttpRequest() {
    curl_close($this->httpRequest);
    $this->httpRequest = NULL;
  }
}
