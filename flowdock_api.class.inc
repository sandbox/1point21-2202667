<?php
/**
 * @file
 * Contains \FlowdockAPI.
 */

/**
 * This is the Flowdock API base class you should extend
 * this class to build custom implementations
 */
abstract class FlowdockApi {
  /**
   * The options Flowdock User API Token which is only used for messages and comments.
   *
   * @var string
   */
  protected $userApiToken;

  /**
   * The Flowdock Flow API Token.
   *
   * @var string
   */
  protected $flowApiToken;

  /**
   * The Flow endpoint path.
   *
   * @var string
   */
  protected $flowsPath = 'flows';

  /**
   * The organizations endpoint path.
   *
   * @var string
   */
  protected $organizationsPath = 'organizations';

  /**
   * The host name for the Flowdock REST API.
   *
   * @var string
   */
  protected $host = 'api.flowdock.com';

  /**
   * The communication scheme.
   *
   * @var string
   */
  protected $httpRequestScheme = 'https';

  /**
   * The optional organization parameter name.
   *
   * @var string
   */
  protected $org;

  /**
   * The optional flow parameter name.
   *
   * @var string
   */
  protected $flow;

  /**
   * The optional message is to globally work with a message.
   *
   * @var int
   */
  protected $messageId;

  /**
   * The optional comment id to globally work with a comment.
   *
   * @var int
   */
  protected $commentId;

  /**
   * The internal http request object e.g. a curl object.
   *
   * @var object
   */
  protected $httpRequest;

  /**
   * An error generated by the HTTP request.
   *
   * @var string
   */
  protected $httpRequestError;

  /**
   * The last response from an http request object.
   *
   * @var object
   */
  protected $httpRequestResponse;

  /**
   * Options for the http request object.
   *
   * @var array
   */
  protected $httpRequestOptions = array();

  const HTTP_GET = 0;
  const HTTP_POST = 1;
  const HTTP_DELETE = 2;
  const HTTP_PUT = 3;
  const TOKEN_TYPE_FLOW = 0;
  const TOKEN_TYPE_USER = 1;

  public function __construct($flow_api_token, $user_api_token = NULL, $org = NULL, $flow = NULL, $message_id = NULL, $comment_id = NULL) {
    $this->setFlowApiToken($flow_api_token);
    $this->setUserApiToken($user_api_token);
    $this->setOrganization($org);
    $this->setFlow($flow);
    $this->setMessageId($message_id);
    $this->setCommentId($comment_id);
  }

  /**
   * Set the organization to use in this object.
   *
   * @param string $org
   *   The organization name parameter.
   */
  public function setOrganization($org) {
    $this->org = $org;
  }

  /**
   * Set the flow to use in this object.
   *
   * @param string $flow
   *   The flow name parameter.
   */
  public function setFlow($flow) {
    $this->flow = $flow;
  }

  /**
   * Set an optional message id to use in this object.
   *
   * @param int $message_id
   *   An int for the message id to optional work with globally.
   */
  public function setMessageId($message_id) {
    $this->messageId = $message_id;
  }

  /**
   * Set an optional comment id to work with in this object.
   *
   * @param int $comment_id
   *   An int for the comment id.
   */
  public function setCommentId($comment_id) {
    $this->commentId = $comment_id;
  }

  /**
   * Set an optional User API Token to work with in this object.
   *
   * @param string $user_api_token
   *   The User API Token to act a specific user.
   */
  public function setUserApiToken($user_api_token) {
    $this->userApiToken = $user_api_token;
  }

  /**
   * Set a Flow API token to work with directly in this object.
   *
   * @param string $flow_api_token
   *   The Flow API Token for a specific flow.
   */
  public function setFlowApiToken($flow_api_token) {
    $this->flowApiToken = $flow_api_token;
  }

  /**
   * Get the base URL to use in webservice calls.
   *
   * @return string
   *   The HTTP scheme and host in URL format.
   */
  protected function getHttpUrl() {
    return $this->httpRequestScheme . '://' . $this->host;
  }

  /**
   * Get the relative Flow path to use in HTTP calls.
   *
   * @return string
   *   The the flow path to use for a specific flow in an organization.
   */
  protected function getHttpFlowsPath() {
    return $this->flowsPath . '/' . $this->org . '/' . $this->flow;
  }

  /**
   * Get the relative Flow message path to use in HTTP calls.
   *
   * @return string
   *   The path to use in call to the message endpoint for a flow.
   */
  protected function getHttpMessagesPath() {
    return $this->getHttpFlowsPath() . '/messages';
  }

  /**
   * Get the relative comment path to use for message in a specific flow.
   *
   * @return string
   *   The comment path
   */
  protected function getHttpCommentsPath($message_id) {
    return $this->getHttpMessagesPath() . '/' . $message_id . '/comments';
  }

  /**
   * Get the Team Inbox path to be used in the HTTP Request object calls
   *
   * @return string
   *   The Team Inbox path for a specific flow.
   */
  protected function getHttpTeamInboxPath() {
    return 'v1/messages/team_inbox/' . $this->flowApiToken;
  }

  /**
   * This method inits an HTTP request object for sendHttpRequest().
   *
   * @param string $path
   *   The HTTP path to call.
   * @param string $method
   *   The method to use when making a call.
   * @param string $data
   *   The data array to use for POST and PUT calls.
   * @param bool $auth
   *   Whether or not authentication should be used when calling the endpoint.
   */
  abstract protected function initHttpRequest($path = NULL, $method = NULL, $data = NULL, $auth = TRUE);

  /**
   * This method send an HTTP request and returns an object.
   *
   * @param string $path
   *   The HTTP path to call.
   * @param string $method
   *   The method to use when making a call.
   * @param string $data
   *   The data array to use for POST and PUT calls.
   * @param bool $auth
   *   Whether or not authentication should be used when calling the endpoint.
   */
  abstract protected function sendHttpRequest($path = NULL, $method = NULL, $data = NULL, $auth = TRUE);

  /**
   * This method closes an HTTP request object after sendHttpRequest().
   */
  abstract protected function closeHttpRequest();

  /**
   * Send data to the endpoint using the HTTP GET method.
   *
   * @param string $path
   *   The path to call.
   * @param bool $auth
   *   Whether or not auth should be used when call the endpoint.
   * @return array
   *   An array of objects return by the endpoint
   */
  protected function get($path = NULL, $auth = TRUE) {
    return $this->sendHttpRequest($path, NULL, NULL, $auth);
  }

  /**
   * Send data to an endpoint using an HTTP POST method.
   *
   * @param string $path
   *   The path to call.
   * @param array $data
   *   An associative array of data to send.
   * @param bool $auth
   *   Whether or not auth should be used when call the endpoint.
   * @return array
   *   An array of objects return by the endpoint
   */
  protected function post($path, $data, $auth = TRUE) {
    return $this->sendHttpRequest($path, self::HTTP_POST, $data, $auth);
  }

  /**
   * Delete data from an enpoint using an HTTP DELETE request.
   *
   * @param string $path
   *   The path to call.
   * @param bool $auth
   *   Whether or not auth should be used when call the endpoint.
   * @return array
   *   An array of objects return by the endpoint
   */
  protected function delete($path, $auth = TRUE) {
    return $this->sendHttpRequest($path, self::HTTP_DELETE, NULL, $auth);
  }

  /**
   * Update data at an endpoint using a PUT request.
   *
   * @param string $path
   *   The path to call.
   * @param array $data
   *   An associative array of data to send.
   * @param bool $auth
   *   Whether or not auth should be used when call the endpoint.
   * @return array
   *   An array of objects return by the endpoint
   */
  protected function put($path, $data, $auth = TRUE) {
    return $this->sendHttpRequest($path, self::HTTP_PUT, $data, $auth);
  }

  /**
   * Get all of the flows for all organizations, requires a User API Token.
   *
   * @return array
   *   An array of objects return by the endpoint.
   */
  public function retrieveAllFlows() {
    return $this->get($this->flowsPath);
  }

  /**
   * Get all of the flows for the current organization.
   * 
   * @return array
   *   An array of objects return by the endpoint.
   */
  public function retrieveFlows() {
    return $this->get($this->getHttpFlowsPath());
  }

  /**
   * Get a list of all organizations available with the User API Token.
   *
   * @return array
   *   An array of objects return by the endpoint.
   */
  public function retrieveOrganizations() {
    return $this->get($this->organizationsPath);
  }

  /**
   * Get information about the flow in the current organization.
   *
   * @return array
   *   An array of objects return by the endpoint.
   */
  public function retrieveFlow() {
    return $this->get($this->getHttpFlowsPath());
  }

  /**
   * Get the messages for the current organization and current flow.
   *
   * @return array
   *   An array of objects return by the endpoint.
   */
  public function retrieveMessages() {
    return $this->get($this->getHttpMessagesPath());
  }

  /**
   * Get the comments for the current organization and current flow.
   *
   * @return array
   *   An array of objects return by the endpoint.
   */
  public function retrieveComments($message_id = NULL) {
    if (empty($message_id)) {
      $message_id = $this->messageId;
    }
    return $this->get($this->getHttpCommentsPath($message_id));
  }

  /**
   * Create a new Team Inbox Message in the flow.
   *
   * @param FlowdockApiTeamInboxMessage $message
   *   A FlowdockApiTeamInboxMessage object containing the message.
   * @return array
   *   An array of objects return by the endpoint.
   */
  public function createTeamInboxMessage(FlowdockApiTeamInboxMessage $message) {
    return $this->post($this->getHttpTeamInboxPath(), $message->toPostArray(), FALSE);
  }

  /**
   * Create a new message for the current organization and current flow.
   *
   * @param string|FlowdockApiMessage $message
   *   Create a message using a string or FlowdockApiMessage
   * @return object
   *   An object returned by the endpoint.
   */
  public function createMessage($message) {
    if (is_string($message)) {
      $message = new FlowdockApiMessage($message);
    }
    return $this->post($this->getHttpMessagesPath(), $message->toPostArray());
  }

  /**
   * Get a message for the current organization and current flow.
   *
   * @param int $message_id
   *   The optional id of the message.
   * @return object
   *   An object returned by the endpoint.
   */
  public function retrieveMessage($message_id = NULL) {
    if (empty($message_id)) {
      $message_id = $this->messageId;
    }
    return $this->get($this->getHttpMessagesPath() . '/' . $message_id);
  }

  /**
   * Update an existing message for the current organization and current flow.
   *
   * @param string|FlowdockApiMessage $message
   *   Create a message using a string or FlowdockApiMessage.
   * @param int message_id
   *   An int for the message_id.
   *   
   * @return object
   *   An empty object.
   */
  public function updateMessage($message, $message_id = NULL) {
    if (is_string($message)) {
      $message = new FlowdockApiMessage($message);
    }
    if (empty($message_id)) {
      $message_id = $this->messageId;
    }
    return $this->put($this->getHttpMessagesPath() . '/' . $message_id, $message->toPostArray());
  }

  /**
   * Delete a message for the current organization and current flow.
   *
   * @param int message_id
   *   An int for the message_id or use the global message_id in the object.
   * @return object
   *   An empty object.
   */
  public function deleteMessage($message_id = NULL) {
    if (empty($message_id)) {
      $message_id = $this->messageId;
    }
    return $this->delete($this->getHttpMessagesPath() . '/' . $message_id);
  }

  /**
   * Create a new comment for the current organization, current flow, and current message.
   *
   * @param string|FlowdockApiMessage $message
   *   Create a comment using a string or FlowdockApiComment
   * @return object
   *   An object returned by the endpoint.
   */
  public function createComment($comment, $message_id = NULL) {
    if (is_string($comment)) {
      $comment = new FlowdockApiComment($comment);
    }
    if (empty($message_id)) {
      $message_id = $this->messageId;
    }
    return $this->post($this->getHttpCommentsPath($message_id), $comment->toPostArray());
  }

  /*
  public function retrieveComment($comment_id = NULL, $message_id = NULL) {
    if (empty($comment_id)) {
      $comment_id = $this->commentId;
    }
    if (empty($message_id)) {
      $message_id = $this->messageId;
    }
    return $this->get($this->getHttpCommentsPath($message_id) . '/' . $comment_id);
  }

  public function updateComment($comment, $comment_id = NULL, $message_id = NULL) {
    if (is_string($comment)) {
      $comment = new FlowdockApiComment($comment);
    }
    if (empty($comment_id)) {
      $comment_id = $this->commentId;
    }
    if (empty($message_id)) {
      $message_id = $this->messageId;
    }
    return $this->put($this->getHttpCommentsPath($message_id) . '/' . $comment_id, $comment->toPostArray());
  }

  public function deleteComment($comment_id = NULL, $message_id = NULL) {
    if (empty($comment_id)) {
      $comment_id = $this->commentId;
    }
    if (empty($message_id)) {
      $message_id = $this->messageId;
    }
    return $this->delete($this->getHttpCommentsPath($message_id) . '/' . $comment_id);
  }
  */
}

/**
 * A Flowdock message which can sent via HTTP
 * PUT or POST to a Flowdock endpoint.
 */
class FlowdockApiTeamInboxMessage {

  /**
   * The name of the source of the message.
   *
   * @var string
   */
  protected $source = '';

  /**
   * The email address to use when replying to the message.
   *
   * @var string
   */
  protected $fromAddress = '';

  /**
   * The subject of the message.
   *
   * @var string
   */
  protected $subject = '';

  /**
   * The content for the message which can be HTML.
   *
   * @var string.
   */
  protected $content = '';

  /**
   * The tags to be include with the message
   * if they are not in the content.
   *
   * @var array
   */
  protected $tags = array();

  public function __construct($source, $from_address, $subject, $content, $tags = NULL) {
    $this->source = $source;
    $this->fromAddress = $from_address;
    $this->subject = $subject;
    $this->content = $content;
    if (!empty($tags)) {
      $this->setTag($tags);
    }
  }

  /**
   * Set one or many tag(s) for the message.
   *
   * @param string|array $tag
   *   A string or array for tags to be set.
   */
  public function setTag($tag) {
    if (is_array($tag)) {
      foreach ($tag as $val) {
        $this->setTag($val);
      }
    }
    else {
      $this->tags[] = $tag;
    }
  }

  /**
   * An Array which can be used in a HTTP POST or PUT message.
   *
   * @return array
   *   The array to be used be the HTTP Request object.
   */
  public function toPostArray() {
    $data = array(
      'source' => $this->source,
      'from_address' => $this->fromAddress,
      'subject' => $this->subject,
      'content' => $this->content
    );
    if (!empty($this->tags)) {
      $tags = array();
      foreach ($this->tags as $tag) {
        $tag = preg_replace('/[,]+/', '', $tag);
        $tags[] = $tag;
      }
      $data['tags'] = implode(',', $tags);
    }
    return $data;
  }
}

/**
 * A Flowdock message to which can sent via a
 * HTTP Request method to a Flowdock endpoint.
 */
class FlowdockApiMessage {
  /**
   * The event type required to create the message.
   *
   * @var string
   */
  protected $event = 'message';

  /**
   * The content of the message which can be HTML.
   *
   * @var string
   */
  protected $content = '';

  /**
   * An array of tags for the message if they are not in the content.
   *
   * @var string.
   */
  protected $tags = array();

  public function __construct($content, $tags = NULL) {
    $this->content = $content;
    if (!empty($tags)) {
      $this->setTag($tags);
    }
  }

  /**
   * Set one or many tags.
   *
   * @param string|array $tag
   *   A string or array of strings.
   */
  public function setTag($tag) {
    if (is_array($tag)) {
      foreach ($tag as $val) {
        $this->setTag($val);
      }
    }
    else {
      $this->tags[] = $tag;
    }
  }

  /**
   * Get an array to be used in a POST or PUT HTTP request.
   *
   * @return array
   *   An array of strings.
   */
  public function toPostArray() {
    $data = array(
      'event' => $this->event,
      'content' => $this->content
    );
    if (!empty($this->tags)) {
      $tags = array();
      foreach ($this->tags as $tag) {
        $tag = preg_replace('/[,]+/', '', $tag);
        $tags[] = $tag;
      }
      $data['tags'] = implode(',', $tags);
    }
    return $data;
  }
}

/**
 * A Flowdock comment message to which can sent via a HTTP Request
 * via HTTP PUT or POST method to a Flowdock endpoint.
 */
class FlowdockApiComment extends FlowdockApiMessage {

  /**
   * {@inheritdoc}
   */
  protected $event = 'comment';
}

/**
 * An exception which is thrown by sendHttpRequest
 * when a response is sent back from an endpoint
 * outside of the 200 range.
 */
class FlowdockApiHttpException extends RuntimeException {}

/**
 * An exception which is thrown by sendHttpRequest
 * when an Authentication faliure is detected.
 */
class FlowdockApiHttpAuthException extends FlowdockApiHttpException {}

